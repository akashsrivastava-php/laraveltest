@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard <span style="float:right;"><a href="{{ url('add') }}">Add Member</a></span></div>

                <div class="card-body">
                    <?php if (Session::get('success') != ''): ?>
                        <div class="alert alert-success" style="text-align:center;"><?php echo Session::get('success'); ?></div>
                    <?php endif;
                    $error = Session::get('error');
                     ?>
                    <?php if ($error != ''): ?>
                        <div class="alert alert-danger" style="text-align:center;"><?php echo $error; ?></div>
                    <?php endif;?>

                    <table class="table">
                        <thead>
                            <tr>
                                <td>Name</td>
                                <td>Contact No.</td>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($list) > 0)
                                @foreach($list as $row)
                                    <tr>
                                        <td>
                                            {{ $row->name }}
                                        </td>
                                        <td>
                                            {{ $row->phone }}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                            <tr><td colspan="2">No Data Found!</td></tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
