<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Members extends Model
{
    protected $table = 'members';
    protected $fillable = ['name', 'phone'];
}