<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Members;
use Session;
use Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $list = Members::all();
        return view('home', compact('list'));
    }

    public function add(){

        return view('add');

    }

    public function insert(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required|digits:10'
        ]);

        $data = array( 'name'=>trim(ucwords($request->name)), 'phone'=>trim($request->phone) );

        Members::create($data);

        return redirect('home')->with('success', 'User registered successfully!');

    }
}
